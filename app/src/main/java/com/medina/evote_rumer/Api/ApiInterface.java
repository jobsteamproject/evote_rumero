package com.medina.evote_rumer.Api;

import com.medina.evote_rumer.Model.Kandidat;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {
    @GET("tampil_binus.php")
    Call<List<Kandidat>> getKandidat();
}
