package com.medina.evote_rumer.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.medina.evote_rumer.Model.Kandidat;
import com.medina.evote_rumer.R;

import java.util.List;

public class KandidatAdapter extends RecyclerView.Adapter<KandidatAdapter.MyviewHolder> {

    private Context context;
    private List<Kandidat> kandidatList;

    public KandidatAdapter(Context context,List<Kandidat> kandidatList) {
        this.context = context;
        this.kandidatList = kandidatList;
    }

    @NonNull
    @Override
    public KandidatAdapter.MyviewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_kandidat,parent,false);
        return new MyviewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull KandidatAdapter.MyviewHolder holder, int position) {

        holder.textViewNama.setText(kandidatList.get(position).getNama());
        holder.textViewKelas.setText(kandidatList.get(position).getKelas());


    }

    @Override
    public int getItemCount() {
        return kandidatList.size();
    }

    public class MyviewHolder extends RecyclerView.ViewHolder {

        TextView textViewNama;
        TextView textViewKelas;
        public MyviewHolder(View itemView) {
            super(itemView);
            textViewNama = itemView.findViewById(R.id.nama);
            textViewKelas = itemView.findViewById(R.id.kelas);
        }
    }
}
