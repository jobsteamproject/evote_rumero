package com.medina.evote_rumer.Model;

import com.google.gson.annotations.SerializedName;

public class Kandidat {

    @SerializedName("nama")
    private String nama;
    @SerializedName("kelas")
    private String kelas;

    public Kandidat(String nama, String kelas){
        this.nama = nama;
        this.kelas = kelas;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }


    @Override
    public String toString() {
        return "Kandidat{" +
                "nama='" + nama + '\'' +
                ", kelas='" + kelas + '\'' +
                '}';
    }
}
