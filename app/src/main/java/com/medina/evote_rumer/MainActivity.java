package com.medina.evote_rumer;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telecom.Call;
import android.util.Log;

import com.medina.evote_rumer.Adapter.KandidatAdapter;
import com.medina.evote_rumer.Api.ApiClient;
import com.medina.evote_rumer.Api.ApiInterface;
import com.medina.evote_rumer.Model.Kandidat;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    List<Kandidat> kandidatList;
    RecyclerView recyclerView;
    KandidatAdapter kandidatAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        kandidatList = new ArrayList<>();

        recyclerView = findViewById(R.id.recyclerview_home);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        retrofit2.Call<List<Kandidat>> call = apiInterface.getKandidat();

        call.enqueue(new Callback<List<Kandidat>>() {
            @Override
            public void onResponse(retrofit2.Call<List<Kandidat>> call, Response<List<Kandidat>> response) {

                kandidatList = response.body();
                kandidatAdapter = new KandidatAdapter(getApplicationContext(),kandidatList);
                recyclerView.setAdapter(kandidatAdapter);
            }

            @Override
            public void onFailure(retrofit2.Call<List<Kandidat>> call, Throwable t) {

            }
        });
    }
}
